package ru.nsu.g.nevolin.util;

public class Vector {

    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static double distance(Vector vec1, Vector vec2) {
        return Math.sqrt(Math.pow(vec1.x - vec2.x, 2) + Math.pow(vec1.y - vec2.y, 2));
    }

    public double x;
    public double y;
}
