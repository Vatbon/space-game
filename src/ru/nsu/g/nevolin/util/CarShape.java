package ru.nsu.g.nevolin.util;

import ru.nsu.g.nevolin.model.entity.Car;

import java.awt.*;

public class CarShape extends Polygon {
    private static Vector[] points = new Vector[3];
    private static Polygon polygon= new Polygon();
    private static void makePoints(Car car){
        int carX = (int) car.getPos().x;
        int carY = (int) car.getPos().y;
        int carWidth = (int) car.getWidth();
        int carHeight = (int) car.getHeight();
        double carRotation = car.getRotation();

        /*Triangle*/
        double gamma = Math.atan(((double) carWidth / 2) / (double) carHeight);
        double t = Math.sqrt(Math.pow((double) carHeight, 2) + Math.pow((double) carWidth / 2, 2));
        double x = (int) (carX + ((double) carHeight / 2) * Math.cos(carRotation));
        double y = (int) (carY + ((double) carHeight / 2) * Math.sin(carRotation));
        points[0] = new Vector(x, y);
        points[1] = new Vector(x - t * Math.cos(carRotation - gamma), y - t * Math.sin(carRotation - gamma));
        points[2] = new Vector(x - t * Math.cos(carRotation + gamma), y - t * Math.sin(carRotation + gamma));
        /*Square*/
        /*double gamma = carRotation + Math.atan(carHeight/carWidth) - Math.PI / 2;
        double r = Math.sqrt(Math.pow(carHeight/2, 2) + Math.pow(carWidth/2 ,2));

        int x = (int)(carX - r * Math.cos(gamma));
        int y = (int)(carY - r * Math.sin(gamma));
        this.addPoint(x, y);

        x += (int)(carHeight * Math.cos(carRotation));
        y += (int)(carHeight * Math.sin(carRotation));
        this.addPoint(x, y);

        carRotation -= Math.PI / 2;
        x += (int)(carWidth * Math.cos(carRotation));
        y += (int)(carWidth * Math.sin(carRotation));
        this.addPoint(x, y);

        carRotation -= Math.PI / 2;
        x += (int)(carHeight * Math.cos(carRotation));
        y += (int)(carHeight * Math.sin(carRotation));
        this.addPoint(x, y);*/
    }
    public static Polygon getCarShape(Car car) {
        makePoints(car);
        polygon.reset();
        polygon.addPoint((int)points[0].x, (int)points[0].y);
        polygon.addPoint((int)points[1].x, (int)points[1].y);
        polygon.addPoint((int)points[2].x, (int)points[2].y);
        return polygon;
    }
    public static Vector[] getPoints(Car car){
        makePoints(car);
        return points;
    }
}
