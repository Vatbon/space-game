package ru.nsu.g.nevolin;

import ru.nsu.g.nevolin.controller.Controller;
import ru.nsu.g.nevolin.model.Model;
import ru.nsu.g.nevolin.view.GameWindow;
import ru.nsu.g.nevolin.view.MainView;

public class Game {

    @Deprecated
    public static void main(String[] args) {
        Model model = new Model();
        Controller controller = new Controller(model);
        GameWindow g = new GameWindow(model, controller);
        MainView mainView = new MainView(model, g);
        mainView.setVisible(true);
    }
}
