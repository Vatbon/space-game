package ru.nsu.g.nevolin.model;

import ru.nsu.g.nevolin.model.entity.Car;
import ru.nsu.g.nevolin.model.entity.Entity;
import ru.nsu.g.nevolin.model.highscores.Highscore;
import ru.nsu.g.nevolin.model.highscores.HighscoreManager;
import ru.nsu.g.nevolin.model.world.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Model {
    public final static int startAccelerate = 0;
    public final static int startBrake = 1;
    public final static int startRotateLeft = 2;
    public final static int startRotateRight = 3;
    public final static int stopAccelerate = 4;
    public final static int stopBrake = 5;
    public final static int stopRotateLeft = 6;
    public final static int stopRotateRight = 7;
    public final static int shoot = 8;

    private World world;
    private long score;
    private HighscoreManager highscoreManager = new HighscoreManager();

    public Model() {
        world = null;
    }

    public Car getCar() {
        return world.getCar();
    }

    public List<Entity> getProjectiles() {
        return world.getProjectiles();
    }

    public List<Entity> getMeteors() {
        return world.getMeteors();
    }

    public void newGame() {
        world = new World(this);
    }

    public long getScore() {
        return world.getScore();
    }

    public void changeState(int e) {
        switch (e) {

            case startAccelerate:
                world.getCar().accelerate = true;
                break;
            case stopAccelerate:
                world.getCar().accelerate = false;
                break;
            case startBrake:
                world.getCar().brake = true;
                break;
            case stopBrake:
                world.getCar().brake = false;
                break;
            case startRotateRight:
                world.getCar().rotateRight = true;
                break;
            case stopRotateRight:
                world.getCar().rotateRight = false;
                break;
            case startRotateLeft:
                world.getCar().rotateLeft = true;
                break;
            case stopRotateLeft:
                world.getCar().rotateLeft = false;
                break;
            case shoot:
                world.makeShot();
        }
    }

    public void newScore(long score) {
        this.score = score;
    }

    public HashMap<String, Long> getHighscores() {
        HashMap<String, Long> highscores = new HashMap<>();
        ArrayList<Highscore> highscoreArrayList = highscoreManager.getHighscores();
        for (Highscore highscore : highscoreArrayList) {
            highscores.put(highscore.getName(), highscore.getScore());
        }
        return highscores;
    }

    public boolean getGameOver() {
        return World.gameOver;
    }

    public void setNewScore(String nickName) {
        highscoreManager.setNewScore(nickName, score);
    }
}
