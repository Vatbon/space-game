package ru.nsu.g.nevolin.model.world;

import ru.nsu.g.nevolin.model.entity.*;
import ru.nsu.g.nevolin.util.CarShape;
import ru.nsu.g.nevolin.util.Vector;

import java.util.LinkedList;
import java.util.List;

public class MotionController {
    public static void update(World world) {
        Car car = world.getCar();
        car.move();

        if (car.getPos().x > World.borderX)
            car.getPos().x = 0;
        if (car.getPos().y > World.borderY)
            car.getPos().y = 0;
        if (car.getPos().x < 0)
            car.getPos().x = World.borderX;
        if (car.getPos().y < 0) {
            car.getPos().y = World.borderY;
        }
        LinkedList<Entity> entities = new LinkedList<>();
        entities.addAll(world.getMeteors());
        entities.addAll(world.getProjectiles());
        checkBounds(entities);
        entities.forEach(Entity::move);
        checkCollisions(world, car, world.getProjectiles(), world.getMeteors());

    }

    private static void checkBounds(List<Entity> entities) {
        LinkedList<Entity> filterEntities = new LinkedList<>(entities);
        filterEntities.
                forEach(e -> {
                    if (e.getPos().x - e.getRadius() > World.borderX || e.getPos().x + e.getRadius() < 0
                            || e.getPos().y - e.getRadius() > World.borderY || e.getPos().y + e.getRadius() < 0) {
                        entities.remove(e);
                    }
                });
    }

    private static void checkCollisions(World world, Car car, List<Entity> projectiles, List<Entity> meteors) {
        for (Entity meteor : meteors) {
            Vector[] carPoints = CarShape.getPoints(car);
            for (Vector carPoint : carPoints) {
                if (Vector.distance(carPoint, meteor.getPos()) < meteor.getRadius())
                    world.gameOver();
            }
        }
        for (Entity projectile : new LinkedList<>(projectiles)) {
            for (Entity meteor : new LinkedList<>(meteors)) {
                if (Vector.distance(projectile.getPos(), meteor.getPos()) < meteor.getRadius() + projectile.getRadius()) {
                    if (((Meteor) meteor).getLevel() > 0)
                        world.collapseMeteor((Meteor) meteor);
                    meteors.remove(meteor);
                    projectiles.remove(projectile);
                    world.increaseScore(world.getReward());
                }
            }
        }

    }
}
