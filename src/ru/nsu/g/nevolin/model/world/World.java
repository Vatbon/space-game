package ru.nsu.g.nevolin.model.world;

import ru.nsu.g.nevolin.model.Model;
import ru.nsu.g.nevolin.model.entity.Car;
import ru.nsu.g.nevolin.model.entity.Entity;
import ru.nsu.g.nevolin.model.entity.Projectile;
import ru.nsu.g.nevolin.util.Vector;
import ru.nsu.g.nevolin.model.entity.Meteor;

import java.util.*;

public class World {
    public static boolean gameOver = false;
    private final Model model;
    private Car car;
    private List<Entity> projectiles;
    private static List<Entity> meteors;

    private static long score = 0;
    private static long reward = 100;
    public static final int borderX = 800;
    public static final int borderY = 600;
    private final int updateInterval = 11;

    private final double chanceOfMeteorToSpawn = 0.04;
    private final double chanceOfMeteorToDirectToPlayer = 0.1;
    private long step = 0;
    private long whenShot = 0, whenScore = 0;
    private long rapidScore = 30;//in steps
    private long firePeriod = 30;//in steps

    public World(Model model) {
        gameOver = false;
        score = 0;
        this.model = model;
        init();
    }

    public void gameOver() {
        gameOver = true;
    }

    public void collapseMeteor(Meteor meteor) {
        int numOfMeteors = new Random().nextInt(2) + new Random().nextInt(3);
        for (int i = 0; i < numOfMeteors; i++)
            meteors.add(new Meteor(meteor));
    }

    public void init() {
        car = new Car(new Vector(borderX/2, borderY/2), new Vector(0, 0), 0);
        projectiles = new LinkedList<>();
        meteors = new LinkedList<>();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (gameOver)
                    cancel();
                update();
            }
        }, 0, updateInterval);
    }

    public void update() {
        if (!gameOver) {
            MotionController.update(this);
            step += 1;
            if (step - whenScore > rapidScore) {
                whenScore = step;
                score += 1;
            }
            if (new Random().nextDouble() < chanceOfMeteorToSpawn){
                Meteor meteor = new Meteor();
                meteors.add(meteor);
                if (new Random().nextDouble() < chanceOfMeteorToDirectToPlayer)
                    meteor.setRotation(Math.atan((car.getPos().y - meteor.getPos().y) / (car.getPos().x - meteor.getPos().x)));
            }
        } else {
            model.newScore(score);
        }
    }

    public Car getCar() {
        return car;
    }

    public List<Entity> getProjectiles() {
        return projectiles;
    }

    public void makeShot() {
        if (step - whenShot > firePeriod) {
            whenShot = step;
            projectiles.add(new Projectile(car.getPos(), car.getRotation()));
        }
    }

    public void increaseScore(long a) {
        score += a;
    }

    public List<Entity> getMeteors() {
        return meteors;
    }

    public long getScore() {
        return score;
    }

    public long getReward() {
        return reward;
    }
}
