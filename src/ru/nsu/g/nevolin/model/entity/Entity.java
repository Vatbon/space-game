package ru.nsu.g.nevolin.model.entity;

import ru.nsu.g.nevolin.util.Vector;

public interface Entity {
    default void move() {
    }

    Vector getPos();

    double getRadius();
}
