package ru.nsu.g.nevolin.model.entity;

import ru.nsu.g.nevolin.util.Vector;

public abstract class AbstractEntity implements Entity{
    protected Vector pos;
    protected double rotation;
    protected Vector speed;
    public AbstractEntity(){
        pos = new Vector(0,0);
        rotation = 0;
        speed = new Vector(0,0);
    }
}
