package ru.nsu.g.nevolin.model.entity;

import ru.nsu.g.nevolin.util.Vector;

public class Car extends AbstractEntity {
    public boolean rotateRight = false;
    public boolean rotateLeft = false;
    public boolean accelerate = false;
    public boolean brake = false;

    private final double epsilon = 0.045;
    private final double linearAcceleration = 0.15;
    private final double angularAcceleration = 0.06;
    private final double frictionC = 0.965;

    private final double width = 11;
    private final double height = 20;

    public Car(Vector pos, Vector speed, double rotation) {
        this.pos = pos;
        this.speed = speed;
        this.rotation = rotation;
    }

    public void move() {
        if (rotateLeft)
            rotation -= angularAcceleration;
        if (rotateRight)
            rotation += angularAcceleration;
        speed.x *= frictionC;
        speed.y *= frictionC;
        if (accelerate) {
            speed.x += linearAcceleration * Math.cos(rotation);
            speed.y += linearAcceleration * Math.sin(rotation);
        }

        if (Math.abs(speed.x) < epsilon)
            speed.x = 0;
        if (Math.abs(speed.y) < epsilon)
            speed.y = 0;

        pos.x += speed.x;
        pos.y += speed.y;
    }

    public Vector getPos() {
        return pos;
    }

    @Override
    public double getRadius() {
        return width;
    }

    public void setPos(Vector pos) {
        this.pos = pos;
    }

    public double getRotation() {
        return rotation;
    }

    public void setSpeed(Vector speed) {
        this.speed = speed;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }
}
