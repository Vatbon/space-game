package ru.nsu.g.nevolin.model.entity;

import ru.nsu.g.nevolin.util.Vector;

public class Projectile extends AbstractEntity {
    private final double radius = 4;

    private final double speed = 7;

    public Projectile(Vector pos, double rotation) {
        super();
        this.pos.x = pos.x;
        this.pos.y = pos.y;
        this.rotation = rotation;
    }

    public void move() {
        pos.x += speed * Math.cos(rotation);
        pos.y += speed * Math.sin(rotation);
    }

    public Vector getPos() {
        return pos;
    }

    public double getRadius() {
        return radius;
    }
}
