package ru.nsu.g.nevolin.model.entity;

import ru.nsu.g.nevolin.util.Vector;
import ru.nsu.g.nevolin.model.world.World;

import java.util.Random;

public class Meteor extends AbstractEntity {
    private double radius;
    private int level;

    private double speed;

    public Meteor(Meteor meteor){
        super();
        Random random = new Random();
        pos.x = meteor.getPos().x;
        pos.y = meteor.getPos().y;
        level = meteor.getLevel() - 1;
        radius = 20 + 20 * level;
        rotation = random.nextDouble() * Math.PI;
        speed = 1 + random.nextDouble();
    }

    public Meteor(){
        super();
        Random random = new Random();
        int side = random.nextInt(4);
        switch (side){
            /*Top*/case 0:
                pos.y = 0;
                pos.x = random.nextInt(World.borderX);
                break;
            /*Right*/case 1:
                pos.y = random.nextInt(World.borderY);
                pos.x = World.borderX;
                break;
            /*Bottom*/case 2:
                pos.y = World.borderY;
                pos.x = random.nextInt(World.borderX);
                break;
            /*Left*/case 3:
                pos.y = random.nextInt(World.borderY);
                pos.x = 0;
                break;
        }
        rotation = random.nextDouble() * Math.PI;
        level = random.nextInt(3);
        radius = 20 + 20 * level;
        speed = 1 + random.nextDouble();
    }

    public void move(){
        pos.x += speed * Math.cos(rotation);
        pos.y += speed * Math.sin(rotation);
    }

    public Vector getPos() {
        return pos;
    }

    public double getRadius() {
        return radius;
    }

    public int getLevel() {
        return level;
    }

    public void setRotation(double rotation){
        this.rotation = rotation;
    }
}