package ru.nsu.g.nevolin.model.highscores;

import java.io.*;
import java.util.ArrayList;

public class HighscoreManager implements Serializable {
    private File highscoresFile = new File("highscores.txt");
    private ArrayList<Highscore> highscores = new ArrayList<>();

    public ArrayList<Highscore> getHighscores() {
        if (highscoresFile.length() > 0) {
            try {
                ObjectInputStream fw = new ObjectInputStream(new FileInputStream(highscoresFile));
                highscores = (ArrayList<Highscore>) fw.readObject();
                highscores.sort((o1, o2) -> Long.compare(o1.getScore(), o2.getScore()));
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return highscores;
    }

    public void setNewScore(String nickName, long score) {
        ObjectOutputStream fw;
        highscores.add(new Highscore(nickName, score));
        try {
            fw = new ObjectOutputStream(new FileOutputStream(highscoresFile));
            fw.writeObject(highscores);
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
