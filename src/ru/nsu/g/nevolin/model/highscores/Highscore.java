package ru.nsu.g.nevolin.model.highscores;

import java.io.Serializable;

public class Highscore implements Serializable{
    static final long serialVersionUID = 1L;
    private String name;
    private long score;

    public Highscore(String name, long score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public long getScore() {
        return score;
    }
}
