package ru.nsu.g.nevolin.controller;

import ru.nsu.g.nevolin.model.Model;

import java.awt.event.KeyEvent;

public class Controller {
    private Model model;

    public Controller(Model model) {
        this.model = model;
    }

    public void keyPressed(int keyCode) {
        switch (keyCode) {
            case KeyEvent.VK_UP:
                model.changeState(Model.startAccelerate);
                break;
            case KeyEvent.VK_DOWN:
                model.changeState(Model.startBrake);
                break;
            case KeyEvent.VK_RIGHT:
                model.changeState(Model.startRotateRight);
                break;
            case KeyEvent.VK_LEFT:
                model.changeState(Model.startRotateLeft);
                break;
            case KeyEvent.VK_SPACE:
                model.changeState(Model.shoot);
                break;
        }
    }

    public void keyReleased(int keyCode) {
        switch (keyCode) {
            case KeyEvent.VK_UP:
                model.changeState(Model.stopAccelerate);
                break;
            case KeyEvent.VK_DOWN:
                model.changeState(Model.stopBrake);
                break;
            case KeyEvent.VK_RIGHT:
                model.changeState(Model.stopRotateRight);
                break;
            case KeyEvent.VK_LEFT:
                model.changeState(Model.stopRotateLeft);
                break;
        }
    }
}
