package ru.nsu.g.nevolin.view;

import ru.nsu.g.nevolin.controller.Controller;
import ru.nsu.g.nevolin.model.Model;
import ru.nsu.g.nevolin.model.entity.Entity;
import ru.nsu.g.nevolin.util.CarShape;
import ru.nsu.g.nevolin.model.world.World;

import javax.sound.sampled.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class GameWindow extends JFrame {
    private Model model;
    private Controller controller;

    private class GameView extends JPanel {
        private double x = 0;
        private double y = 0;
        private boolean toDraw;
        private int drawInterval = 33;

        private GameView() {
            this.setSize(World.borderX, World.borderY);
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    SwingUtilities.invokeLater(() -> repaint(drawInterval));
                }
            }, 0, drawInterval);
            requestFocusInWindow();
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.setColor(Color.WHITE);
            g.fillPolygon(CarShape.getCarShape(model.getCar()));
            g.setColor(Color.WHITE);
            synchronized (model.getProjectiles()) {
                for (Entity projectile : new ArrayList<>(model.getProjectiles())) {
                    drawEntity(g, projectile);
                }
            }
            synchronized (model.getMeteors()) {
                g.setColor(Color.RED);
                for (Entity meteor : new ArrayList<>(model.getMeteors())) {
                    drawEntity(g, meteor);
                }
            }
            g.setColor(Color.WHITE);
            g.drawString("Score: " + model.getScore(), 0, 10);
        }
    }

    private void drawEntity(Graphics g, Entity entity) {
        int radius = (int) entity.getRadius();
        g.drawOval((int) entity.getPos().x - radius, (int) entity.getPos().y - radius, radius * 2, radius * 2);
    }

    private class ArrowsKeyListener implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {
            controller.keyPressed(e.getKeyCode());
        }

        @Override
        public void keyReleased(KeyEvent e) {
            controller.keyReleased(e.getKeyCode());
        }
    }

    public GameWindow(Model model, Controller controller) {
        super();
        this.model = model;
        this.controller = controller;
        this.setBounds(320, 240, World.borderX, World.borderY);
        this.setMinimumSize(new Dimension(World.borderX, World.borderY));
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new CardLayout());
        this.setFocusable(false);
        GameView g = new GameView();
        g.addKeyListener(new ArrowsKeyListener());
        g.setFocusable(true);
        this.add(g);
        this.pack();
        new Thread(() -> new MakeSound().playSound("sound/8-bit.wav")).start();
    }

    public class MakeSound {

        private final int BUFFER_SIZE = 128000;
        private File soundFile;
        private AudioInputStream audioStream;
        private AudioFormat audioFormat;
        private SourceDataLine sourceLine;

        private void playSound(String filename) {

            try {
                soundFile = new File(filename);
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
            }

            while (true) {
                try {
                    audioStream = AudioSystem.getAudioInputStream(soundFile);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }

                audioFormat = audioStream.getFormat();

                DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
                try {
                    sourceLine = (SourceDataLine) AudioSystem.getLine(info);
                    sourceLine.open(audioFormat);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }


                sourceLine.start();

                int nBytesRead = 0;
                byte[] abData = new byte[BUFFER_SIZE];
                while (nBytesRead != -1) {
                    try {
                        nBytesRead = audioStream.read(abData, 0, abData.length);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (nBytesRead >= 0) {
                        int nBytesWritten = sourceLine.write(abData, 0, nBytesRead);
                    }
                }

                sourceLine.drain();
                sourceLine.close();
            }
        }
    }

}
