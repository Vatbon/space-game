package ru.nsu.g.nevolin.view;

import ru.nsu.g.nevolin.model.Model;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class MainView extends JFrame {
    private boolean gameStarted = false;
    private boolean appExited = false;
    private Model model;
    private GameWindow gameWindow;

    class ButtonNewGameListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            gameStarted = true;
            model.newGame();
            gameWindow.setVisible(true);
        }
    }

    class ButtonHighscoresListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            HashMap<String, Long> highscores = model.getHighscores();
            StringBuilder str = new StringBuilder("");
            class Highscore {
                private String nick;
                private Long score;

                private Highscore(String nick, Long score) {
                    this.nick = nick;
                    this.score = score;
                }
            }
            ArrayList<Highscore> highscoresArrayList = new ArrayList<>();
            for (Map.Entry<String, Long> entry : highscores.entrySet()) {
                highscoresArrayList.add(new Highscore(entry.getKey(), entry.getValue()));
            }
            highscoresArrayList.sort((o1, o2) -> -o1.score.compareTo(o2.score));
            for (int i = 0; i < highscoresArrayList.size() && i < 10; i++) {
                str.append(i + 1).append(") ").append(highscoresArrayList.get(i).nick).append(":").append(highscoresArrayList.get(i).score).append("\n");
            }
            JOptionPane.showMessageDialog((Component) e.getSource(), str.toString());
        }
    }

    class ButtonExitListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            appExited = true;
            System.exit(0);
        }
    }

    public MainView(Model model, GameWindow gw) {
        super("Cosmic mouse");
        java.util.Timer timer = new java.util.Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (model.getGameOver() && gameStarted) {
                    gameWindow.setVisible(false);
                    setNewScore();
                    gameStarted = false;
                }
            }
        }, 0, 500);
        this.model = model;
        this.gameWindow = gw;
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(320, 240));
        Container container = this.getContentPane();
        container.setLayout(new GridBagLayout());
        JButton new_game = new JButton("New Game");
        new_game.setSize(100, 40);
        new_game.addActionListener(new ButtonNewGameListener());
        JButton high_scores = new JButton("Highscores");
        high_scores.setSize(100, 40);
        high_scores.addActionListener(new ButtonHighscoresListener());
        JButton exit = new JButton("Exit");
        exit.setSize(100, 40);
        exit.addActionListener(new ButtonExitListener());

        GridBagConstraints g = new GridBagConstraints();
        g.gridx = 0;
        g.gridy = 0;
        container.add(new_game, g);
        g = new GridBagConstraints();
        g.gridx = 0;
        g.gridy = 1;
        container.add(high_scores, g);
        g = new GridBagConstraints();
        g.gridx = 0;
        g.gridy = 2;
        container.add(exit, g);
        pack();
    }

    private void setNewScore() {
        String nickName = JOptionPane.showInputDialog("Input your name");
        model.setNewScore(nickName);
    }
}
